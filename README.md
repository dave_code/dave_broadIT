# Lecture Material for IT starter with python

This is the lecture material for IT starter with python.

## Table of Contents

- [Download](#download)
- [Usage](#usage)
- [Support](#support)
- [Contributing](#contributing)
- [License](#license)

## Download 

Download to your project directory:

```sh
git clone https://gitlab.com/dave_code/dave_broadIT.git 
```

## Usage

- open PDF file with your PDF viewer

## Support

Please contact to [dream@www.funcoding.xyz](mailto:dream@funcoding.xyz).

## Contributing

Welecome to submit a merge request.


## License 

Copyright 잔재미코딩(www.fun-coding.org) All rights reserved.

